function dotfiles --wraps=git --description 'Wrapper around the dotfiles git bare repository for easy management.'
  git --git-dir=$HOME/.config/dotfiles.git --work-tree=$HOME $argv
end

