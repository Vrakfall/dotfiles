function lla --wraps=ls --description 'List all contents of directory using long format'
ls -alh $argv
end
